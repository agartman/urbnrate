import React, { Component } from "react"
import Head from "next/head"

class SearchAddress extends Component {
  render() {
    return (
      <div>
        <Head>
          <script src="/static/autocomplete.js" />
        </Head>
        <input type="text" placeholder="Address here" id="searchAddress" />
        <style jsx>{`
          #searchAddress {
            width: 300px;
            padding: 10px;
            border: none;
            border-bottom: solid 2px #c9c9c9;
            -webkit-transition: border 0.3s;
            -moz-transition: border 0.3s;
            -o-transition: border 0.3s;
            transition: border 0.3s;
          }
          #searchAddress:focus,
          #searchAddress.focus {
            border-bottom: solid 2px #969696;
          }
        `}</style>
      </div>
    )
  }
}
export default SearchAddress
