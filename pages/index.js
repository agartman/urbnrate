import Menu from "../components/menu"
import React, { Component } from "react"
import Styles from "../components/styles"
import SearchAddress from "../components/searchAddress"

class Index extends Component {
  render() {
    return (
      <div className="wrapper">
        <header className="header">URBNRate!</header>
        <aside className="sidebar">Sidebar</aside>
        <article className="content">
          <h1>Search for a place</h1>
          <SearchAddress />
        </article>
        <footer className="footer">Copyright URBNRate.com</footer>
        <Styles />
        <script
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCavIV6WpBg1OTQFHvQLMiQapY_xHoEBJo&libraries=places&callback=autocomplete"
          async
          defer
        />
      </div>
    )
  }
}

export default Index
